module.exports = function() 
{
	var events = require('events');     
    
    var emitter = new events.EventEmitter();

    var formatters = [];
    var services = [];
	
	function assocServiceFormatter(service, formatter)
	{
		services.push(service);
		formatters.push(formatter);		
	}

	function formatData(serviceData)
	{
		for (var i = 0; i < services.length; i++) 
		{
			if (serviceData.serviceName == services[i]().getServiceName())
			{				
				var formattedData = formatters[i].format(serviceData.dataType, serviceData.data);	
				sendFormattedData(formattedData, serviceData.id);
			}
		}
	}

	function formatError(errData)
	{
		for (var i = 0; i < services.length; i++) 
		{
			if (errData.serviceName == services[i]().getServiceName())
			{
				console.log(errData);							
				sendFormattedData(errData.message, errData.id);
			}
		}
	}

	function sendFormattedData(text, requestId)
	{		
		if (typeof text === 'string')
		{
			var data =
	        {
	        	text: text,
	        	requestId: requestId, //is this coupling ok?
				//This is here to account for the fact that the slack service gets rid of requests of its stored array 
				//once dealt with but for the case of request data being sent in parts (as below) we need to be aware of the final section
	        	isFinalSection: true 
	        }

	        emitter.emit("formattedData", data);
		}
		else
		{
			for (var i = 0; i < text.length; i++) 
			{
				var isFinalSection = false;
				if (i == text.length -1)				
					isFinalSection = true;
				var data =
		        {
		        	text: text[i],
		        	requestId: requestId, //is this coupling ok?
		        	//This is here to account for the fact that the slack service gets rid of requests of its stored array 
					//once dealt with but for the case of request data being sent in parts (as below) we need to be aware of the final section
		        	isFinalSection: isFinalSection
		        }
		        emitter.emit("formattedData", data);				
			}
		}
	}    

	emitter.assocServiceFormatter = assocServiceFormatter;
	emitter.formatData = formatData;
	emitter.formatError = formatError;

    return emitter;

}();