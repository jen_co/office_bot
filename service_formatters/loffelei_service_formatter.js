module.exports = function() 
{	
    function format(dataType, data)
    {
    	var returnText = "";
    	switch(dataType)
        {
            case "menuTodayData":
                returnText = formatMenuTodayData(data);
                break;
            case "menuAnyDayData":
                returnText = formatMenuAnyDayData(data);
                break;                                       
        }
        return returnText;
    }

    function formatMenuTodayData(data)
    {    	
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* brings you *LOFFELEI MENU*!\n--------------------------------------------------------------------------------------------\n";
         
        text += data.menu + "<" + data.url + "|Loffelei website>";                                         

    	return text;
    }

    function formatMenuAnyDayData(data)
    {
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* answers your request for *LOFFELEI MENU* for " + data.day + ":\n--------------------------------------------------------------------------------------------\n";    	    	

        text += data.menu + "<" + data.url + "|Loffelei website>";                                         

        return text;
    }   
    
    return {
    	format: format
    }; 

}();