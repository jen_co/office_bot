module.exports = function() 
{	

    function format(dataType, data)
    {
    	var returnText = "";
    	switch(dataType)
        {
            case "fullListingData":
                returnText = formatFullListingData(data);
                break;
            case "cinemasForMovieData":
                returnText = formatCinemasForMovieData(data);
                break;
            case "moviesForCinemaData":
                returnText = formatMoviesForCinemaData(data);
                break;   
            case "allCinemasData":
                returnText = formatAllCinemasData(data); 
                break;
            case "allMoviesData":
                returnText = formatAllMoviesData(data); 
                break;                                                        
        }
        return returnText;
    }

    function formatFullListingData(data)
    {
    	var movieTextArray = [];
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* brings you *ENGLISH MOVIE* Listings!\n";

    	for (var i = 0; i < data.cinemas.length; i++) 
    	{
    		if (text.length > 5000)
    		{   			
    			movieTextArray.push(text);
    			text = "";
    		}

    		var cinema = data.cinemas[i];

    		var cinemaText = "\n*<" + cinema.url  + " |" + cinema.name + ">*"; 
                       
            text += "--------------------------------------------------------------------------------------------\n" + cinemaText;
            text += "\n ---------------------------------------------------\n";

            for (var j = 0; j < cinema.movies.length; j++) 
            {
            	var movie = cinema.movies[j];
            	var movieText = "<" + movie.url + " |" + movie.name + "> \n" + movie.times.join() +"\n"+"\n";
            	text += movieText;
            }
    	}

    	if (text.length > 1)
    	{
    		movieTextArray.push(text);
    	}

    	return movieTextArray;
    }

    function formatCinemasForMovieData(data)
    {
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* answers your request for cinemas playing *";
    	
    	text += "<" + data.movie.url + " |" + data.movie.name + ">*:\n";    	


    	for (var i = 0; i < data.cinemas.length; i++) 
        {
        	text += "--------------------------------------------------------------------------------------------\n";
        	text += "<" + data.cinemas[i].url  + " |" + data.cinemas[i].name + ">\n" + data.cinemas[i].times.join() + "\n";  
        }

       return text;
    }

    function formatMoviesForCinemaData(data)
    {
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* answers your request for movies playing at *";
    	
    	text += "<" + data.cinema.url  + " |" + data.cinema.name + ">*:\n";    


    	for (var i = 0; i < data.movies.length; i++) 
        {
        	text += "--------------------------------------------------------------------------------------------\n";
        	text += "<" + data.movies[i].url + " |" + data.movies[i].name + ">\n" + data.movies[i].times.join() + "\n";  
        }

       return text;
    }

    function formatAllCinemasData(data)
    {
        var textArray = [];
        var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* answers your request for all cinemas\n";                     

        for (var i = 0; i < data.cinemas.length; i++) 
        {
            if (text.length > 5000)
            {               
                textArray.push(text);
                text = "";
            }

            text += "--------------------------------------------------------------------------------------------\n";
            text += "<" + data.cinemas[i].url  + " |" + data.cinemas[i].name + ">\n";  
        }

        if (text.length > 1)
        {
            textArray.push(text);
        }

       return textArray;
    }

    function formatAllMoviesData(data)
    {
        var textArray = [];
        var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* answers your request for all movies playing\n";                

        for (var i = 0; i < data.movies.length; i++) 
        {

            if (text.length > 5000)
            {               
                textArray.push(text);
                text = "";
            }

            text += "--------------------------------------------------------------------------------------------\n";
            text += "<" + data.movies[i].url + " |" + data.movies[i].name + ">\n";  
        }

        if (text.length > 1)
        {
            textArray.push(text);
        }

       return textArray;
    }
    
    return {
    	format: format
    }; 

}();