module.exports = function() 
{	
    function format(dataType, data)
    {
    	var returnText = "";
    	switch(dataType)
        {
            case "wowData":
                returnText = formatWowData(data);
                break;
            case "wodData":
                returnText = formatWodData(data);
                break;
            case "defData":
                returnText = formatDefData(data);
                break;
            case "synData":
                returnText = formatSynData(data);
                break;                                               
        }
        return returnText;
    }

    function formatWowData(data)
    {    	
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* brings you *WORD OF THE WEEK*!\n--------------------------------------------------------------------------------------------\n";

    	text += "*" + data.word + "*: " + data.definition + "\n *Example:* " + data.example;

    	return text;
    }

    function formatWodData(data)
    {
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* answers your request for *WORD OF THE DAY*!\n--------------------------------------------------------------------------------------------\n";

    	text += "*" + data.word + "*: " + data.definition + "\n *Example:* " + data.example;

        return text;
    }

    function formatDefData(data)
    {
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* answers your definition request for *"; 

    	text += data.word + "*:\n--------------------------------------------------------------------------------------------\n";

    	for (var i = 0; i < data.definitions.length; i++) 
		{
			text += "- " + data.definitions[i] + "\n";
		}

		return text;
    }

    function formatSynData(data)
    {
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* answers your synonym request for *"; 

    	text += data.word + "*:\n--------------------------------------------------------------------------------------------\n";

    	for (var i = 0; i < data.synonyms.length; i++) 
		{
			text += "- " + data.synonyms[i] + "\n";
		}

		return text;
    }
    
    return {
    	format: format
    }; 

}();