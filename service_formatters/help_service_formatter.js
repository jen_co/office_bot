module.exports = function() 
{	
    function format(dataType, data)
    {
    	var returnText = "";
    	switch(dataType)
        {
            case "helpData":
                returnText = formatHelpDataAll(data);
                break;            
            default:
                returnText = formatHelpDataSpecific(data);               
        }
        return returnText;
    }

    function formatHelpDataAll(data)
    {    	
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* brings you the following services:\n";

        for (var i = 0; i < data.services.length; i++) 
        {
            text += "--------------------------------------------------------------------------------------------\n*";
            text += data.services[i].name;
            text += "*\n ---------------------------------------------------\n";
            text += "- " + data.services[i].description + "\n";
            text += "- For more help on this service type: *'" + data.services[i].help + "'*\n";
        }

    	return text;
    }

    function formatHelpDataSpecific(data)
    {
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* brings you information on how to use the *";
        text += data.name + "*:\n--------------------------------------------------------------------------------------------\n";

        text += data.description + "\n";

        for (var i = 0; i < data.requestOptions.length; i++) 
        {
            text += "--------------------------------------------------------------------------------------------\n";
            text += "- *'" + data.requestOptions[i].requestFormat + "'*\n";
            if (data.requestOptions[i].example.length > 1)
                text += "- " + "example usage: '" + data.requestOptions[i].example + "'\n";
            text += "- " + data.requestOptions[i].returnData + "\n";
        }

        return text;
    }     
    
    return {
    	format: format
    }; 

}();