module.exports = function() 
{	
    function format(dataType, data)
    {
    	var returnText = "";
    	switch(dataType)
        {
            case "lipsumData":
                returnText = formatLipsumData(data);
                break;                                                
        }
        return returnText;
    }

    function formatLipsumData(data)
    {    	
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* brings you some dummy text!:\n--------------------------------------------------------------------------------------------\n";

        text += data.lipsum;

    	return text;
    }   
    
    return {
    	format: format
    }; 

}();