module.exports = function() 
{	

    function format(dataType, data)
    {
    	var returnText = "";
    	switch(dataType)
        {
            case "fullListingData":
                returnText = formatFullListingData(data);
                break;
            case "timesForMovieData":
                returnText = formatTimesForMovieData(data);
                break;
            case "allMoviesData":
                returnText = formatAllMoviesData(data); 
                break;                                                        
        }
        return returnText;
    }

    function formatFullListingData(data)
    {
    	var movieTextArray = [];
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* brings you *LABIA MOVIE* Listings!\n";

    	for (var i = 0; i < data.movies.length; i++) 
    	{
    		if (text.length > 5000)
    		{   			
    			movieTextArray.push(text);
    			text = "";
    		}

    		var movie = data.movies[i];
    		
            var movieText = "<" + movie.url + " |" + movie.title + "> \n" + movie.times.join(", ") +"\n"+"\n";
            text += movieText;
    	}

    	if (text.length > 1)
    	{
    		movieTextArray.push(text);
    	}

    	return movieTextArray;
    }


    function formatTimesForMovieData(data)
    {
    	var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* answers your request for movies times*";
    	
    	text += "<" + data.url  + " |" + data.title + ">*:\n";    
        text += data.times.join(", ") + "\n";

        return text;
    }


    function formatAllMoviesData(data)
    {
        var textArray = [];
        var text = "--------------------------------------------------------------------------------------------\n*OFFICE BOT* answers your request for all movies playing\n";                

        for (var i = 0; i < data.movies.length; i++) 
        {

            if (text.length > 5000)
            {               
                textArray.push(text);
                text = "";
            }

            text += "--------------------------------------------------------------------------------------------\n";
            text += "<" + data.movies[i].url + " |" + data.movies[i].title + ">\n";  
        }

        if (text.length > 1)
        {
            textArray.push(text);
        }

       return textArray;
    }
    
    return {
    	format: format
    }; 

}();