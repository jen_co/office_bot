module.exports = function()
{
	var request = require('request');
	var events = require('events');		
	var emitter = new events.EventEmitter();

	var serviceName = "helpService"; 

	function execute(args)
	{		
		var helpInfo = 
		{
			services:
			[
				/*{
					name: "Loffelei Service",
					description: "Provides menu information for the Loffelei restaurant",
					help: "officebot help loff",
					requestOptions:
					[
						{
							requestFormat: "officebot loff",
							example: "",
							returnData: "Returns the Loffelei menu for today"
						},
						{
							requestFormat: "officebot loff {insert day of the week}",
							example: "officebot lof wednesday",
							returnData: "Returns the Loffelei menu for the request day"
						}
					]
				},*/
				{
					name: "Word Service",
					description: "Provides word of the day, definitions and synonyms",
					help: "officebot help word",
					requestOptions:
					[
						{
							requestFormat: "officebot word wod",
							example: "",
							returnData: "Returns a word of the day with defintion and example"
						},
						{
							requestFormat: "officebot word def {insert word}",
							example: "officebot word def pumpernickel",
							returnData: "Returns definitions of the requested word"
						},
						{
							requestFormat: "officebot word syn {insert word}",
							example: "officebot word syn happy",
							returnData: "Returns synonyms of the requested word"
						}
					]
				},
				/*{
					name: "Movie Service",
					description: "Provides English movie listing information",
					help: "officebot help movies",
					requestOptions:
					[
						{
							requestFormat: "officebot movies",
							example: "",
							returnData: "Returns all the English movies showing on the day requested. If requested on Thursday or on the weekend, will return Thurs, Fri, Sat, Sun"
						},
						{
							requestFormat: "officebot movies getcinemas",
							example: "",
							returnData: "Returns a list of all cinemas"
						},
						{
							requestFormat: "officebot movies getmovies",
							example: "",
							returnData: "Returns a list of all movies playing"
						},
						{
							requestFormat: "officebot movies getcinemas {insert movie title}",
							example: "officebot movies getcinemas Citizenfour",
							returnData: "Returns all the cinemas playing the requested movie, with times"
						},
						{
							requestFormat: "officebot movies getmovies {insert cinema name}",
							example: "officebot movies getmovies Rollberg",
							returnData: "Returns all the movies showing at the requested cinema, with times"
						}
					]
				},*/
				{
					name: "Lorem Ipsum Service",
					description: "Provides a chunk of dummy text",
					help: "officebot help lipsum",
					requestOptions:
					[
						{
							requestFormat: "officebot lipsum",
							example: "",
							returnData: "Returns the dummy text"
						}
					]
				},
				{
					name: "Labia Service",
					description: "Provides Labia movie listing information",
					help: "officebot help labia",
					requestOptions:
					[
						{
							requestFormat: "officebot labia",
							example: "",
							returnData: "Returns all the movies and movie info currently showing at the Labia"
						},
						{
							requestFormat: "officebot movies getmovies",
							example: "",
							returnData: "Returns a list of all movies playing"
						},
						{
							requestFormat: "officebot labia gettimes {insert move title here}",
							example: "",
							returnData: "Returns all times for the given movie"
						}
					]
				},
			]
		}

        if (args && args.length == 0)
        	sendData(helpInfo, "helpData");

        if (args && args[0])
        {
        	switch(args[0].toLowerCase()) 
			{
			   // case "loff":
			   // 	sendData(helpInfo.services[0], "loffHelpData");			        
			   //     break;
			    case "word":
			    	sendData(helpInfo.services[0], "wordHelpData");			        
			        break;
			    //case "movies":			    
			    //	sendData(helpInfo.services[2], "movieHelpData");			        			        
			    //    break;    
			    case "lipsum":
			        sendData(helpInfo.services[1], "lipsumHelpData");			        			        			        
			        break;  
		        case "labia":
		        	sendData(helpInfo.services[2], "labiaHelpData");			        			        			        
		        break;       			    
			    default:
			    	setUnknownArgMessage(args);			    				    				    				        			        			        
			}
        }						
	}

	function getServiceName()
    {
       return serviceName;
    }

    function sendData(data, dataType)
	{

		console.log("at help service");
		console.log(data);

		var serviceData =
        {
            serviceName: serviceName,
            dataType: dataType,
            data: data
        };
		
		emitter.emit("data", serviceData);	
	}

	function sendError(errMsg)
	{
		var errorData =
        {
            serviceName: serviceName,            
            message: errMsg
        };
		
		emitter.emit("error", errorData);	
	}

	function setUnknownArgMessage(args)
	{
		var errMsg = "Sorry, the service '" + args[0] + "' does not exist. Please check your spelling and or type 'officebot help' for a list of available services.";    	
    	sendError(errMsg);    	
	}
	
	emitter.execute = execute;
	emitter.getServiceName = getServiceName;
	return emitter;
};