module.exports = function()
{

	var request = require('request');
	var cheerio = require('cheerio');
	var events = require('events');		
	
	var emitter = new events.EventEmitter();
	
	var loffeleiUrl = 'http://die-loeffelei.de/start/unsere-wochenkarte';		

	var hasExecuted = false;	

	var serviceName = "loffService";	

	//checks if its the right time of day/week to execute the service
	function isExecTime(currentTime) 
	{
		//Everyday at 12pm
		if (currentTime.getHours() == 12 )
		{
			return true;
		}
		return false;
	}

	function execute(args) 
	{
		var day = new Date().getDay();
		
		var weekday = new Array(7);
		weekday[0]=  "....///";
		weekday[1] = "Montag";
		weekday[2] = "Dienstag";
		weekday[3] = "Mittwoch";
		weekday[4] = "Donnerstag";
		weekday[5] = "Freitag";
		weekday[6] = "....///";


		if (args && args[0])
		{			
			switch(args[0].toLowerCase()) 
			{
			    case "monday":
			        day = 1;
			        break;
			    case "tuesday":
			        day = 2;
			        break;
			    case "wednesday":
			        day = 3;
			        break;    
			    case "thursday":
			        day = 4;
			        break;        
			    case "friday":
			        day = 5;
			        break; 
			    default:
			    	day = null;
			}
		}

		if (day)
			getMenuData(args, weekday, day);
		else
			sendUnknownArgMessage(args);						
	}

	function getMenuData(args, weekday, day)
	{
		request(loffeleiUrl, function(error, response, html) 
		{
		
			if (!error) 
			{
				var $ = cheerio.load(html);								

				var context = $("#post-5"); 
				var tables = context.find("table");

				tables.each(function() 
				{
		    		if ( $(this).text().indexOf(weekday[day]) > -1 )
		    		{

		    			if (!$(this).text() || $(this).text() == "")
						{
							sendRequestFailure();
							return;
						}								

		    			var menuInfo = 
		    			{
		    				menu: $(this).text(),
		    				url: loffeleiUrl
		    			};

		    			if (!args || (args && args.length == 0))
		    				sendData(menuInfo, "menuTodayData");		    			
		    			
				        if (args && args[0])
				        {
				        	menuInfo.day = args[0];
				        	sendData(menuInfo, "menuAnyDayData");
				        }				        			    			
		    		} 		        		
				});
			}
			else
				sendRequestFailure();
		});
	}

	function getServiceName()
    {
       return serviceName;
    }

    function sendData(data, dataType)
	{
		var serviceData =
        {
            serviceName: serviceName,
            dataType: dataType,
            data: data
        };
		
		emitter.emit("data", serviceData);	
	}

	function sendError(errMsg)
	{
		var errorData =
        {
            serviceName: serviceName,            
            message: errMsg
        };
		
		emitter.emit("error", errorData);	
	}

	function sendRequestFailure()
	{			
		sendError("Sorry but the data for your Loffelei Service request could not be retrieved. Please try again later.");
	}	

	function sendUnknownArgMessage(args)
	{		
    	var errMsg = "Sorry, Loffelei Service expects a day of the week from Monday to Friday and doesn't understand '" + args[0] + "'. Please check your spelling and or type 'officebot help loff' for more info.";    	
    	sendError(errMsg);
	}	
	
	emitter.isExecTime = isExecTime;
	emitter.execute = execute;
	emitter.hasExecuted = hasExecuted;
	emitter.getServiceName = getServiceName;
	
	return emitter;
};