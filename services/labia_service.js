module.exports = function() 
{
    var request = require('request');
    var cheerio = require('cheerio');
    var events = require('events');     
    
    var emitter = new events.EventEmitter(); 

    var movieUrl = 'http://www.labia.co.za/showing.php';   

    var hasExecuted = false;

    var allMovieInfoSplitCounter = 0;

    var moviesInfo = 
    {
        movies: []            
       /* 
        [
            {
                title: 
                description:
                url:
                times:                         
                [
                ]
                
            }
        ]
        */
    };

    var serviceName = "labiaService";
   

    //checks if its the right time of day/week to execute the service
    function isExecTime(currentTime) 
    {
        //friday at 3pm  which on heroku is  13 because it appears to be 2 hours behind....    
        if (currentTime.getHours() == 13 && currentTime.getDay() == 5 )
        {
            return true;
        }
        return false;
    }    

    function execute(args) 
    {
        request(movieUrl, function(error, response, html) 
        {      

            if (!error) 
            {
                var $ = cheerio.load(html);

                var context = $("body");                

                var tables = context.find("table");

                if (tables.length > 0)
                {
                    var data = tables.eq(1);// [1].find("tr");
                    if (data.length > 0)
                    {
                        var rows = data.find("td");
                        var counter = 0;
                        rows.each(function() 
                        {
                            if (counter > 3) //ignore the screen rows
                            {
                                //
                                var movieIndex = getMovieByTitle($(this).find('a').text().trim());
                               
                                if (movieIndex == -1) //check if it doesnt already exist 
                                {
                                     var movie = {
                                        title:  "",
                                        url: "",
                                        times: []
                                    };
                                    movie.title = $(this).find('a').text().trim();
                                    movie.url = "http://www.labia.co.za/reviews.php/" +  $(this).find('a').attr('href');
                                    var content = $(this).clone();
                                    content.find('a').remove();
                                    content.find('br').remove();
                                    movie.times.push(content.text().replace('\n', "").replace("new", "").trim());
                                    if (movie.title != "")
                                        moviesInfo.movies.push(movie);
                                }
                                else
                                {
                                    var content = $(this).clone();
                                    content.find('a').remove();
                                    content.find('br').remove();
                                    moviesInfo.movies[movieIndex].times.push(content.text().replace('\n', "").replace("new", "").trim()); 
                                }
                               
                            }
                            counter++;
                        });
                    }
                }

               for (var i = 0; i < moviesInfo.movies.length; i++) 
               {
                   console.log(moviesInfo.movies[i]);
               }          

                //only send all if no args
                if (!args || (args && args.length == 0))
                    sendData(moviesInfo, "fullListingData");
                else
                {
                    switch(args[0].toLowerCase())
                    {
                        case "gettimes":
                            if (args[1])
                                getTimesForMovie(args.slice(1).join(" "));
                            break;
                        case "getmovies":
                                getAllMovies();
                            break;
                        default:
                            sendUnknownArgMessage(args);                           
                    }
                }



            }
            else
                sendRequestFailure();
        });

    } 

    function getMovieByTitle(title)
    {
        for (var i = 0; i < moviesInfo.movies.length; i++) 
        {
            if (title.toLowerCase() === moviesInfo.movies[i].title.toLowerCase())
                return i;
        }

        return -1;
    }

    function getTimesForMovie(movieTitle)
    {
        var index = getMovieByTitle(movieTitle);
        if (index > -1)
        {
            sendData(moviesInfo.movies[index], "timesForMovieData");      
        }
        else
            sendRequestFailure(movieTitle);
        
    }


    function getAllMovies()
    {
        sendData(moviesInfo, "allMoviesData");        
    }  


    function getServiceName()
    {
       return serviceName;
    }

    function sendData(data, dataType)
    {
        var serviceData =
        {
            serviceName: serviceName,
            dataType: dataType,
            data: data
        };
        
        emitter.emit("data", serviceData);  
    }

    function sendError(errMsg)
    {
        var errorData =
        {
            serviceName: serviceName,            
            message: errMsg
        };
        
        emitter.emit("error", errorData);   
    }

    function sendUnknownArgMessage(args)
    {       
        var errMsg = "Sorry, Movie Service does not have a function for '" + args[0] + "'. Please check your spelling and or type 'officebot help movies' for a list of available functions.";        
        sendError(errMsg);
    }

    function sendRequestFailure(requestArg)
    {        
        if (requestArg)
            sendError("Sorry but the data for your Movie Service request for " +  requestArg + " could not be retrieved. Please check the spelling of your request or try again later.");
        else
            sendError("Sorry but the data for your Movie Service request could not be retrieved. Please try again later.");
    }

    emitter.isExecTime = isExecTime;
    emitter.execute = execute;
    emitter.hasExecuted = hasExecuted; 
    emitter.getServiceName = getServiceName;

    return emitter;
};