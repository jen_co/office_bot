module.exports = function() 
{	
	var request = require('request');
	var cheerio = require('cheerio');		
	var events = require('events');		
	
	var emitter = new events.EventEmitter();

	var wowUrl = 'http://api.wordnik.com/v4/words.json/wordOfTheDay?api_key=950173baf143414e9fa05037c700e695e7d207c45592a2546';	
	//will replace {||||} with the requested word
	var definitionUrl = 'http://api.wordnik.com/v4/word.json/{||||}/definitions?limit=5&sourceDictionaries=webster&useCanonical=false&includeTags=false&api_key=950173baf143414e9fa05037c700e695e7d207c45592a2546'
	var synonymUrl = 'http://api.wordnik.com/v4/word.json/{||||}/relatedWords?useCanonical=false&relationshipTypes=synonym&limitPerRelationshipType=10&api_key=950173baf143414e9fa05037c700e695e7d207c45592a2546'

	var hasExecuted = false;	

	var serviceName = "wowService";	
	
	
	//checks if its the right time of day/week to execute the service
	function isExecTime(currentTime) 
	{
		console.log(currentTime);
		
		//Monday at 4pm
		if (currentTime.getHours() == 14 && currentTime.getDay() == 1)
		{
			return true;
		}
		return false;
	}	

	function execute(args) 
	{		
		
		if (!args) 
			getWowData(args, wowUrl);

		if (args && args[0])
        {
        	switch(args[0].toLowerCase())
        	{
        		case "wod":
        			getWowData(args, wowUrl);
        			break;
        		case "def":
        			if (args[1])
					{
						var url = definitionUrl.replace("{||||}", args[1]);
						getDefData(args, url);		
					}
					else
						sendMissingArgMessage(args[0]);
        			break;
        		case "syn":
        			if (args[1])
					{
						var url = synonymUrl.replace("{||||}", args[1]);
						getSynData(args, url);		
					}
					else
						sendMissingArgMessage(args[0]);	
        			break;
        		default:
        			sendUnknownArgMessage(args);		
        	}
        }
        else if (args && args.length == 0) 	
        	sendUnknownArgMessage(args);
	}		

	function getWowData(args, url)
	{
		request(url, function(error, response, data) 
		{
			if (!error) 
			{
				var wowData = JSON.parse(data);

				if (!wowData || !wowData.definitions[0] || !wowData.word)
				{
					sendRequestFailure("word of the day");
					return;
				}
				
				var wordInfo = 
				{
					word: wowData.word,
					definition: wowData.definitions[0].text,
					example: wowData.examples[0].text
				};

				if (!args)
					sendData(wordInfo, "wowData");	
				else
					sendData(wordInfo, "wodData");												
			}
			else
				sendRequestFailure("word of the day");
		});		
	}

	function getDefData(args, url)
	{
		request(url, function(error, response, data)
		{
			if (!error) 
			{
				var defData = JSON.parse(data);	

				if (!defData || defData.length == 0)
				{
					sendRequestFailure("definition", args[1]);
					return;
				}

				var wordInfo = 
				{
					word: args[1],
					definitions: []
				}				

				for (var i = 0; i < defData.length; i++) 
				{
					wordInfo.definitions.push(defData[i].text);
				}

				sendData(wordInfo, "defData");				
			}
			else
				sendRequestFailure("definition", args[1]);

		}); 		
	}

	function getSynData(args, url)
	{
		request(url, function(error, response, data)
		{
			if (!error) 
			{
				var synData = JSON.parse(data);

				if (!synData || synData.length == 0)
				{
					sendRequestFailure("synonyms", args[1]);
					return;
				}
				
				var wordInfo = 
				{
					word: args[1],
					synonyms: []

				};				

				for (var i = 0; i < synData[0].words.length; i++) 
				{
					wordInfo.synonyms.push(synData[0].words[i]);
				}

				sendData(wordInfo, "synData");				
			}
			else
				sendRequestFailure("synonym", args[1]);
		});	
		
	}

	function sendUnknownArgMessage(args)
	{

    	var errMsg = "Sorry, Word Service does not have a function for '" + args[0] + "'. Please check your spelling and or type 'officebot help word' for a list of available functions";

    	if (!args[0])
    		errMsg = "Sorry, Word Service needs a function specified. Please type 'officebot help word' for a list of available functions";

    	sendError(errMsg);
	}

	function sendRequestFailure(request, requestArg)
	{
		if (requestArg)
			sendError("Sorry but the data for your Word Service request for " + request + " of '" +  requestArg + "' could not be retrieved. Please check the spelling of your requested word or try again later.");
		else
			sendError("Sorry but the data for your Word Service request '" + request + "' could not be retrieved. Please try again later.");
	}

	function sendMissingArgMessage(serviceFunction)
	{
		var errMsg = "Sorry, the function " + serviceFunction + " requires a word. Type 'officebot help word' for the correct request format for this function.";   	
    	sendError(errMsg);
	}

	function sendData(data, dataType)
	{
		var serviceData =
        {
            serviceName: serviceName,
            dataType: dataType,
            data: data
        };
		
		emitter.emit("data", serviceData);	
	}

	function sendError(errMsg)
	{
		var errorData =
        {
            serviceName: serviceName,            
            message: errMsg
        };
		
		emitter.emit("error", errorData);	
	}

	function getServiceName()
    {
       return serviceName;
    }	

	emitter.isExecTime = isExecTime;
	emitter.execute = execute;
	emitter.hasExecuted = hasExecuted;		
	emitter.getServiceName = getServiceName;

	return emitter;
};