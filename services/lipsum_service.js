module.exports = function()
{
	var request = require('request');
	var events = require('events');		
	var emitter = new events.EventEmitter();

	var serviceName = "lipsumService"; 

	function execute(args)
	{
		request("http://loripsum.net/api/10/medium/plaintext", function(error, response, html) 
		{									
			if (!error) 
			{
				var lipsumData = 
				{
					lipsum: html
				};

				sendData(lipsumData, "lipsumData");				
			}
			else
				sendRequestFailure();
		});
	}

	function getServiceName()
    {
       return serviceName;
    }

    function sendData(data, dataType)
    {
        var serviceData =
        {
            serviceName: serviceName,
            dataType: dataType,
            data: data
        };
        
        emitter.emit("data", serviceData);  
    }

    function sendError(errMsg)
    {
        var errorData =
        {
            serviceName: serviceName,            
            message: errMsg
        };
        
        emitter.emit("error", errorData);   
    } 
	
    function sendRequestFailure()
    {                
        sendError("Sorry but the data for your Lorem Ipsum Service request could not be retrieved. Please try again later.");
    }


	emitter.execute = execute;
	emitter.getServiceName = getServiceName;
	return emitter;
};