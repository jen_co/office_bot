module.exports = function() 
{
    var request = require('request');
    var cheerio = require('cheerio');
    var events = require('events');     
    
    var emitter = new events.EventEmitter(); 

    var movieUrl = 'http://berlin.angloinfo.com/whatson/movies';   

    var hasExecuted = false;

    var allMovieInfoSplitCounter = 0;

    var moviesInfo = 
    {
        cinemas: []            
       /* [
            {
                name: 
                url:
                movies: 
                [
                    {
                        name: 
                        description:
                        url:
                        times:                         
                        [
                        ]
                    }
                ]
            }
        ]*/
    };

    var serviceName = "movieService";
   

    //checks if its the right time of day/week to execute the service
    function isExecTime(currentTime) 
    {
        //Thursday at 5pm        
        if (currentTime.getHours() == 17 && currentTime.getDay() == 4 )
        {
            return true;
        }
        return false;
    }    

    function execute(args) 
    {
        request(movieUrl, function(error, response, html) 
        {      

            if (!error) 
            {
                var $ = cheerio.load(html);

                var context = $(".company-listings");                
                var listElements = context.find("li");
                
                var currentCinemaIndex = 0;                                
                
                listElements.each(function() 
                {                    
                    if ($(this).has("h5").length == 1)
                    {

                        var cinemaUrl = $(this).find("a").attr("href");
                        var cinemaUrlShort = "http://google.com";  //if for some reason html structure changes...make hyperlink google
                        if(cinemaUrl.indexOf("url/") > -1)
                            cinemaUrlShort = "http://" + cinemaUrl.substring(cinemaUrl.indexOf("url/") + 4);
                        
                        var cinemaName = $(this).find("h5").text();
                        //this is a hack because the stupid ass website has malformed html which means that their stupid ass h5 tag is closed with an h4 tag :(
                        var hackCinemaName = "cinema name unknown";
                        if(cinemaName.indexOf("At:") > -1)
                            hackCinemaName = cinemaName.substring(0, cinemaName.indexOf("At:")).replace("\n", "");                            

                        var cinema = 
                        {
                            name: hackCinemaName,
                            url: cinemaUrlShort,
                            movies: []
                        };
                        moviesInfo.cinemas.push(cinema);
                        currentCinemaIndex = moviesInfo.cinemas.length - 1;                                                                                                   
                    }
                    else
                    {
                        var imdbUrl = $(this).find("h4").find("a").attr("href");
                        
                        var imdbUrlShort = "http://imdb.com";//if for some reason html structure changes...make hyperlink imdb
                        if (imdbUrl.indexOf("url/") > -1)
                            imdbUrlShort = "http://" + imdbUrl.substring(imdbUrl.indexOf("url/") + 4);                                                

                        var movieTitle = $(this).find("h4").find("a").text();
                        var movieTitleAndDescr = $(this).find("h4").text();
                        var movieDescription = movieTitleAndDescr.substring(movieTitleAndDescr.indexOf("-") + 1);
                        var movieTimes = $(this).find(".description").text().split(",");

                        var movie = 
                        {
                            name: movieTitle,
                            url: imdbUrlShort,
                            description: movieDescription,
                            times: movieTimes
                        };                        
                        moviesInfo.cinemas[currentCinemaIndex].movies.push(movie);
                    }                                                    
                });                                

                
                //only send all if no args
                if (!args || (args && args.length == 0))
                    sendData(moviesInfo, "fullListingData");
                else
                {
                    switch(args[0].toLowerCase())
                    {
                        case "getcinemas":
                            if (args[1])
                                getCinemasFromMovie(args.slice(1).join(" "));
                            else
                                getAllCinemas();
                            break;
                        case "getmovies":
                            if (args[1])
                                getMoviesFromCinema(args.slice(1).join(" "));
                            else
                                getAllMovies();
                            break;
                        default:
                            sendUnknownArgMessage(args);                           
                    }
                }

                                                               
            }
            else
                sendRequestFailure();
        });

    } 

    function getAllCinemas()
    {
        var allCinemasInfo =
        {
            cinemas: []
        };

        for (var i = 0; i < moviesInfo.cinemas.length; i++) 
        {
            var cinema =
            {
                name: moviesInfo.cinemas[i].name, 
                url: moviesInfo.cinemas[i].url
            };

            allCinemasInfo.cinemas.push(cinema);    
        }

        sendData(allCinemasInfo, "allCinemasData");        
    }   

    function getCinemasFromMovie(movieName)
    {
        var cinemasForMovieInfo = 
        {
            movie:
            {
                name: movieName
            },
            cinemas: []
        };

        for (var i = 0; i < moviesInfo.cinemas.length; i++) 
        {            
            for (var j = 0; j < moviesInfo.cinemas[i].movies.length; j++) 
            {
                if (moviesInfo.cinemas[i].movies[j].name.toLowerCase() == movieName.toLowerCase())
                {
                    cinemasForMovieInfo.movie.url = moviesInfo.cinemas[i].movies[j].url;
                    var cinemaName = moviesInfo.cinemas[i].name;
                    var movieTimes = moviesInfo.cinemas[i].movies[j].times;
                    var url = moviesInfo.cinemas[i].url;
                    var cinema = 
                    {
                        name: cinemaName,
                        url: url,
                        times: movieTimes
                    };
                    cinemasForMovieInfo.cinemas.push(cinema);
                }                    
            }
        }

        if (cinemasForMovieInfo.cinemas.length == 0)
            sendRequestFailure(movieName);
        else
            sendData(cinemasForMovieInfo, "cinemasForMovieData");
        
    }

    function getAllMovies()
    {
        var allMoviesInfo =
        {
            movies: []
        };

        //keep track of names of movies that we have already added so dont get repeats
        var addedMovieNames = [];

        for (var i = 0; i < moviesInfo.cinemas.length; i++) 
        {            
            for (var j = 0; j < moviesInfo.cinemas[i].movies.length; j++) 
            {
                var movieName = moviesInfo.cinemas[i].movies[j].name;
                //check if movie not yet added to the list
                if (addedMovieNames.indexOf(movieName) == -1)
                {
                    var movie =
                    {
                        name: movieName,
                        url: moviesInfo.cinemas[i].movies[j].url
                    };

                    allMoviesInfo.movies.push(movie); 
                    addedMovieNames.push(movieName);
                }                
            }                          
        }

        sendData(allMoviesInfo, "allMoviesData");        

    }  

    function getMoviesFromCinema(cinemaName)
    {
        var moviesForCinemaInfo =
        {
            cinema:
            {
                name: cinemaName
            },
            movies: []
        };

        for (var i = 0; i < moviesInfo.cinemas.length; i++) 
        {
            if (moviesInfo.cinemas[i].name.toLowerCase().indexOf(cinemaName.toLowerCase()) > -1)
            {
                moviesForCinemaInfo.cinema.url = moviesInfo.cinemas[i].url;
                for (var j = 0; j < moviesInfo.cinemas[i].movies.length; j++) 
                {
                    var movieName = moviesInfo.cinemas[i].movies[j].name;
                    var movieTimes = moviesInfo.cinemas[i].movies[j].times;
                    var url = moviesInfo.cinemas[i].movies[j].url;
                    var movie =
                    {
                        name: movieName,
                        url: url,                        
                        times: movieTimes
                    };
                    moviesForCinemaInfo.movies.push(movie);
                }
            }
        }

        if (moviesForCinemaInfo.movies.length == 0)
            sendRequestFailure(cinemaName);
        else
            sendData(moviesForCinemaInfo, "moviesForCinemaData");        
    }  

    function getServiceName()
    {
       return serviceName;
    }

    function sendData(data, dataType)
    {
        var serviceData =
        {
            serviceName: serviceName,
            dataType: dataType,
            data: data
        };
        
        emitter.emit("data", serviceData);  
    }

    function sendError(errMsg)
    {
        var errorData =
        {
            serviceName: serviceName,            
            message: errMsg
        };
        
        emitter.emit("error", errorData);   
    }

    function sendUnknownArgMessage(args)
    {       
        var errMsg = "Sorry, Movie Service does not have a function for '" + args[0] + "'. Please check your spelling and or type 'officebot help movies' for a list of available functions.";        
        sendError(errMsg);
    }

    function sendRequestFailure(requestArg)
    {        
        if (requestArg)
            sendError("Sorry but the data for your Movie Service request for " +  requestArg + " could not be retrieved. Please check the spelling of your request or try again later.");
        else
            sendError("Sorry but the data for your Movie Service request could not be retrieved. Please try again later.");
    }

    emitter.isExecTime = isExecTime;
    emitter.execute = execute;
    emitter.hasExecuted = hasExecuted; 
    emitter.getServiceName = getServiceName;

    return emitter;
};