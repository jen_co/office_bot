module.exports = function()
{
	var request = require('request');
	var events = require('events');		
	
	var serviceHandler = require('./service_handler.js');

	var emitter = new events.EventEmitter();			

	var timedServices = [];	
	var requestServices = [];	
	var serviceKeys = [];

	function registerTimedService(service)
	{
		timedServices.push(service);
		//remove first in case already listening
		service.removeListener("data", handleData);
		service.on("data", handleData);
	}

	function registerRequestService(serviceKey, service) 
	{
		requestServices.push(service);
		serviceKeys.push(serviceKey);	
	}

	function deregisterTimedService(service)
	{
		//etc run through list and checck if sevrvice then remove
	}

	function runTimedServices(isDebug)
	{
		var currentTime = new Date();
		for (var i = 0; i < timedServices.length; i++) 
		{
			var service = timedServices[i];  

			var canExec = service.isExecTime(currentTime);
			if (isDebug)
				canExec = true;
			if (canExec && !service.hasExecuted)
			{
				service.execute();
				service.hasExecuted = true;
			}

		}
	}		

	function restartTimedServices()
	{
		for (var i = 0; i < timedServices.length; i++) 
		{
			var service = new timedServices[i]();
			service.hasExecuted = false;		
		}
	}

	function runRequestService(data)
	{
		var serviceKey = data.serviceKey;
		var serviceKeyIndex = serviceKeys.indexOf(serviceKey);		
		if (serviceKeyIndex > -1)
		{
			var service = new requestServices[serviceKeyIndex]();
			
			var serviceHandlerInstance = new serviceHandler();			
			
			serviceHandlerInstance.removeListener("data", handleData);
			serviceHandlerInstance.on("data", handleData);

			serviceHandlerInstance.removeListener("error", handleError);
			serviceHandlerInstance.on("error", handleError);

			serviceHandlerInstance.associate(data.id, service, data.args);	
		}	
	}

	function handleData(data)
	{		
		emitter.emit("data", data); 
	}

	function handleError(data)
	{		
		emitter.emit("error", data);
	}

	emitter.registerTimedService = registerTimedService;
	emitter.deregisterTimedService = deregisterTimedService;
	emitter.restartTimedServices = restartTimedServices;
	emitter.runTimedServices = runTimedServices;
	emitter.registerRequestService = registerRequestService;
	emitter.runRequestService = runRequestService;

	return emitter;

}();


