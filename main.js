var request = require('request');

var serviceManager = require("./service_manager.js");

var slackService = require("./slack_service.js");

var formatter = require("./formatter.js");

var loffService = require("./services/loffelei_service.js");
var wowService = require("./services/wow_service.js");
var movieService = require("./services/movie_service.js");
var lipService = require("./services/lipsum_service.js");
var helpService = require("./services/help_service.js");
var labiaService = require("./services/labia_service.js");

var movieServiceFormatter = require("./service_formatters/movie_service_formatter.js");
var wowServiceFormatter = require("./service_formatters/wow_service_formatter.js");
var loffServiceFormatter = require("./service_formatters/loffelei_service_formatter.js");
var lipServiceFormatter = require("./service_formatters/lipsum_service_formatter.js");
var helpServiceFormatter = require("./service_formatters/help_service_formatter.js");
var labiaServiceFormatter = require("./service_formatters/labia_service_formatter.js");


var timerInterval = 600000;

var isDebug = false;

if (isDebug)
{	
	timerInterval = 3000;
}	


slackService.init(isDebug);

slackService.on('request', serviceManager.runRequestService);

serviceManager.on('data', formatter.formatData);
serviceManager.on('error', formatter.formatError);

//serviceManager.registerTimedService(new loffService());
serviceManager.registerTimedService(new wowService());
//serviceManager.registerTimedService(new movieService());
serviceManager.registerTimedService(new labiaService());

//dont pass in instance here as need to create new instances for each request, so can identify each unique request
serviceManager.registerRequestService("word", wowService);
serviceManager.registerRequestService("lipsum", lipService);
//serviceManager.registerRequestService("loff", loffService);
//serviceManager.registerRequestService("movies", movieService);
serviceManager.registerRequestService("help", helpService);
serviceManager.registerRequestService("labia", labiaService);

//formatter.assocServiceFormatter(movieService, movieServiceFormatter);
formatter.assocServiceFormatter(wowService, wowServiceFormatter);
//formatter.assocServiceFormatter(loffService, loffServiceFormatter);
formatter.assocServiceFormatter(lipService, lipServiceFormatter);
formatter.assocServiceFormatter(helpService, helpServiceFormatter);
formatter.assocServiceFormatter(labiaService, labiaServiceFormatter);

formatter.on('formattedData', slackService.postData);

var currentDate = new Date();
var currentDay = currentDate.getDay();
console.log("ready");
console.log(currentDay);
console.log(currentDate.getHours());
setInterval(runServices, timerInterval);

function runServices()
{	
	var day = new Date().getDay();
	if (currentDay != day)
	{
		serviceManager.restartTimedServices();
		currentDay = day;
	}			

	serviceManager.runTimedServices(isDebug);
}


