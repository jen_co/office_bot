module.exports = function() 
{
	var request = require('request');
	var express = require('express');
	var bodyParser = require('body-parser');
	var events = require('events');		
	
	var emitter = new events.EventEmitter();
	var app = express();
	app.use(bodyParser.json()); // for parsing application/json
	app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded	


	var slackUrl = 'https://hooks.slack.com/services/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';//https://hooks.slack.com/services/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';

	var queuedPosts = [];
	var requests = [];

	function init(isDebug)
	{
		
		//if(isDebug)
		//	slackUrl = 'https://hooks.slack.com/services/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';

		listen();
		setTimeout(processQueue, 1000);
	}

	function postData(data)
	{		
		queuedPosts.push(data);
	}

	function processQueue()
	{		
		if (queuedPosts.length > 0)
		{
			//NB TODO have logic that only shifts when succesful but then will also need logic for retrying only 3 times so no infinite loop
			//remove the first item of the array, this shift function returns the shifted object						
			post(queuedPosts.shift());
		}
		else
		{
			setTimeout(processQueue, 1000);
		}
	}	

	function post(data)
	{		
		var postData = 
		{
			text: data.text		
		};
		
		if (data.requestId)
		{
			var index;
			for (var i = 0; i < requests.length; i++) 
			{				
				if (requests[i].id == data.requestId)
				{
					postData.channel = "#officebot";//"@" + requests[i].user;
					if (data.isFinalSection) //This is here to account for the case of a request being sent in parts. Only want to remove the request from the array once all parts have been dealt with
						index = i;
				}		
			}
			//once got the needed info from the requests array can remove it from the array as
			//we cant keep adding to the array without cleaning it up!
			if (index)
				requests.splice(index, 1);
		}					
		
		var options = 
		{
			method: 'post',
			body: postData,
			json: true,
			url: slackUrl
		};
	                       
		request(options, function (err, res, body) 
		{
		    console.log(err);													
			if (err) 
			{
				return setTimeout(processQueue, 1000);
			}
			
			setTimeout(processQueue, 1000);
		});			
	}


	function listen()
	{		
		app.get('/', function(req, res, next) 
		{
			res.send("Hello from officebot :) ");
		});

		app.post('/', function(req, res, next) 
		{
			console.log(req.body);

		  	var textArray = req.body.text.split(" ");
		  	var serviceKey = textArray[1];
		  	var args = textArray.slice(2);

		  	if (serviceKey == null)
		  	{
		  		var jsonResponse =
		  		{
		  			text: "Office Bot can't read minds just yet so please provide a keyword :), or ask me for some help by typing 'officebot help'." ,
					channel: req.body.user_name
		  		};

		  		res.json(jsonResponse);
		  	}
		  	else
		  	{
		  		//this data will go the external service
		  		var dataToExtService = 
				{
					serviceKey: textArray[1],
					args: args,
					id: req.body.timestamp + "|" + req.body.user_id	  					  		
				};

				//this data will be stored in an array to keep track of requests and match them to correct posts back to slack
				var dataToStore = dataToExtService;
				dataToStore.user = req.body.user_name;

				requests.push(dataToStore);
				emitter.emit("request", dataToExtService);	
		  	}	  	   	
		});

		app.listen(3000);//process.env.PORT || 3000);
	}


	emitter.init = init;
	emitter.postData = postData;

	return emitter;
	
}();