module.exports = function()
{
	var events = require('events');		
	
	var emitter = new events.EventEmitter();

	var id;		

	function handleData(data) 
	{
		data.id = id;		
		emitter.emit("data", data);
	}

	function handleError(data) 
	{	
		data.id = id;
		emitter.emit("error", data);
	}

	function associate(requestId, service, args) 
	{
		id = requestId;
		
		service.removeListener("data", handleData);
		service.on("data", handleData);

		service.removeListener("error", handleError);
		service.on("error", handleError);
		
		service.execute(args);
	}

	emitter.associate = associate;
	return emitter;
};//();